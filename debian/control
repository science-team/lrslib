Source: lrslib
Section: math
Priority: optional
Maintainer: David Bremner <bremner@debian.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               libgmp-dev,
               mpi-default-dev
Standards-Version: 4.5.0
Homepage: http://cgm.cs.mcgill.ca/~avis/C/lrs.html
Vcs-Git: https://salsa.debian.org/science-team/lrslib/
Vcs-Browser: https://salsa.debian.org/science-team/lrslib.git

Package: lrslib
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: package to enumerate vertices and extreme rays of a convex polyhedron
 A convex polyhedron is the set of points satisfying a finite family
 of linear inequalities.  The study of the vertices and extreme rays
 of such systems is important and useful in e.g. mathematics and
 optimization.  In a dual interpretation, finding the vertices of a
 (bounded) polyhedron is equivalent to finding the convex hull
 (bounding inequalities) of an (arbitrary dimensional) set of points.
 Lrs (lexicographic reverse search) has two important features that
 can be very important for certain applications: it works in exact
 arithmetic, and it consumes memory proportional to the input, no
 matter how large the output is.

Package: mplrs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: package to enumerate vertices and extreme rays of a convex polyhedron (parallel binary)
 A convex polyhedron is the set of points satisfying a finite family
 of linear inequalities.  The study of the vertices and extreme rays
 of such systems is important and useful in e.g. mathematics and
 optimization.  In a dual interpretation, finding the vertices of a
 (bounded) polyhedron is equivalent to finding the convex hull
 (bounding inequalities) of an (arbitrary dimensional) set of points.
 Lrs (lexicographic reverse search) has two important features that
 can be very important for certain applications: it works in exact
 arithmetic, and it consumes memory proportional to the input, no
 matter how large the output is.
 .
 This package contains the parallel binary mplrs for use with mpi
 
Package: liblrs2
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: package to enumerate vertices and extreme rays (shared libraries)
 A convex polyhedron is the set of points satisfying a finite family
 of linear inequalities.  The study of the vertices and extreme rays
 of such systems is important and useful in e.g. mathematics and
 optimization.  In a dual interpretation, finding the vertices of a
 (bounded) polyhedron is equivalent to finding the convex hull
 (bounding inequalities) of an (arbitrary dimensional) set of points.
 Lrs (lexicographic reverse search) has two important features that
 can be very important for certain applications: it works in exact
 arithmetic, and it consumes memory proportional to the input, no
 matter how large the output is.
 .
 This package contains the (required) shared library.

Package: liblrs-dev
Architecture: any
Depends: liblrs2 (=${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Breaks: liblrsgmp-dev (<< 0.70)
Replaces: liblrsgmp-dev (<< 0.70)
Section: libdevel
Description: package to enumerate vertices and extreme rays (development file)
 A convex polyhedron is the set of points satisfying a finite family
 of linear inequalities.  The study of the vertices and extreme rays
 of such systems is important and useful in e.g. mathematics and
 optimization.  In a dual interpretation, finding the vertices of a
 (bounded) polyhedron is equivalent to finding the convex hull
 (bounding inequalities) of an (arbitrary dimensional) set of points.
 Lrs (lexicographic reverse search) has two important features that
 can be very important for certain applications: it works in exact
 arithmetic, and it consumes memory proportional to the input, no
 matter how large the output is.
 .
 This package contains the optional headers, and a unversioned symlink
 to the library, useful for developers.
